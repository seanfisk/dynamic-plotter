backends Package
================

:mod:`backends` Package
-----------------------

.. automodule:: dynamic_plotter.gui.backends
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`base` Module
------------------

.. automodule:: dynamic_plotter.gui.backends.base
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`matplotlib` Module
------------------------

.. automodule:: dynamic_plotter.gui.backends.matplotlib
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`mayavi` Module
--------------------

.. automodule:: dynamic_plotter.gui.backends.mayavi
    :members:
    :undoc-members:
    :show-inheritance:

