##########
References
##########

.. _argparse: http://docs.python.org/dev/library/argparse.html
.. _PySide: http://www.pyside.org/
.. _Sphinx: http://sphinx.pocoo.org/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _distutils: http://docs.python.org/library/distutils.html
.. _matplotlib: http://matplotlib.sourceforge.net/
.. _Mayavi: http://code.enthought.com/projects/mayavi/

+-------------------------+------------------------------+
|Purpose                  |Library                       |
+=========================+==============================+
|Argument Parsing         |`argparse`_ \(stdlib >= 2.7\) |
+-------------------------+------------------------------+
|Documentation            |`Sphinx`_, `reStructuredText`_|
+-------------------------+------------------------------+
|Distribution             |`distutils`_                  |
+-------------------------+------------------------------+
|Graphical User Interface |`PySide`_                     |
+-------------------------+------------------------------+
|Visualization            |`matplotlib`_, `Mayavi`_      |
+-------------------------+------------------------------+
