core Package
============

:mod:`core` Package
-------------------

.. automodule:: dynamic_plotter.core
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`discretize` Module
------------------------

.. automodule:: dynamic_plotter.core.discretize
    :members:
    :undoc-members:
    :show-inheritance:

