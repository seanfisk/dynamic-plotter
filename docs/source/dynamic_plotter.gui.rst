gui Package
===========

:mod:`gui` Package
------------------

.. automodule:: dynamic_plotter.gui
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`window` Module
--------------------

.. automodule:: dynamic_plotter.gui.window
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    dynamic_plotter.gui.backends

