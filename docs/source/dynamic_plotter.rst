dynamic_plotter Package
=======================

:mod:`dynamic_plotter` Package
------------------------------

.. automodule:: dynamic_plotter.__init__
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`metadata` Module
----------------------

.. automodule:: dynamic_plotter.metadata
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    dynamic_plotter.core
    dynamic_plotter.gui

