""":mod:`dynamic_plotter` -- Graphical representation of functions

.. todo::

    Extend setup script with setuptools.

.. todo::

    Freeze the application and also try for Windows, Debian-based, RPM-based,
    and OS X distribution.

.. todo::

    Upload documentation to something like packages.python.org, github pages, or read
"""

import metadata
__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
