""":mod:dynamic_plotter.gui.backends.base --- Base backend for Matplotlib and
Mayavi
"""

from pyface.qt import QtCore, QtGui

class BackendBaseWidget(QtGui.QWidget):
    """Abstract base class for multiple backend widgets.

    .. todo::

        Use the Abstract Base Classes module (:mod:`abc`)
        
    """
    def graph(self, discrete_values, limits):
        """Graph a 2D or 3D function. Currently a stub in lieu of Kurt's
        uncommited code.

        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 1 or 2
            len(limits) == 1 is 2D graph; len(limits) == 2 is 3D graph.
            The dependent variable is autoscaled.
        """
        raise NotImplementedError('Please implement this method.')
