""":mod:`dynamic_plotter.gui.backends.mayavi` --- Mayavi-specific backend code
"""

# avoid relative import error with mayavi import line
from __future__ import absolute_import

from pyface.qt import QtCore, QtGui
from traits.api import (HasTraits, Instance, on_trait_change, Int, Dict)
from traitsui.api import View, Item
from mayavi.core.ui.api import (MayaviScene, MlabSceneModel, SceneEditor)

TUBE_RADIUS = 0.1
"""Width of the graphed line."""

class Visualization(HasTraits):
    """Mayavi widget which actually does rendering.
    
    .. todo::

        Allow multiple mlab instances.

    """
    scene = Instance(MlabSceneModel, ())

    @on_trait_change('scene.activated')
    def update_plot(self):
        """Function called when view is opened.

        .. todo::

            Decide whether this function is necessary and what to do with it.
            
        """
        pass
        # This function is called when the view is opened. We don't
        # populate the scene when the view is not yet open, as some
        # VTK features require a GLContext.

        # We can do normal mlab calls on the embedded scene.
        # self.scene.mlab.test_points3d()
        # Create the data.
        # from numpy import pi, sin, cos, mgrid
        # dphi, dtheta = pi/250.0, pi/250.0
        # [phi,theta] = mgrid[0:pi+dphi*1.5:dphi,0:2*pi+dtheta*1.5:dtheta]
        # m0 = 4; m1 = 3; m2 = 2; m3 = 3; m4 = 6; m5 = 2; m6 = 6; m7 = 4;
        # r = sin(m0*phi)**m1 + cos(m2*phi)**m3 + sin(m4*theta)**m5 + cos(m6*theta)**m7
        # x = r*sin(phi)*cos(theta)
        # y = r*cos(phi)
        # z = r*sin(phi)*sin(theta)

        # View it.
        # from mayavi import mlab
        # self.scene.mlab.mesh(x, y, z)
        # mlab.show()

    def graph(self, discrete_values, limits):
        """Graph a 3D function. Currently a stub in lieu of Kurt's uncommited
        code. Mayavi only supports 3D graphing. This function currently ignores
        limits.

        .. todo::

            Don't ignore limits.
            
        .. seealso:: :meth:`MayaviWidget.graph()`

        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph (currently ignored)
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 2
            The dependent variable is autoscaled.
        """
        # from mayavi import mlab
        self.scene.mlab.plot3d(
            discrete_values[0],
            discrete_values[1],
            discrete_values[2],
            tube_radius=TUBE_RADIUS,
            )
        self.scene.mlab.axes(
            xlabel='X',
            ylabel='Y',
            zlabel='Z',
            )
        # mlab.show()
        self.scene.mlab.show()

    # the layout of the dialog screated
    view = View(
        Item(
            'scene',
             editor=SceneEditor(scene_class=MayaviScene),
            height=250,
            width=300,
            show_label=False
            ),
            # We need this to resize with the parent widget
        resizable=True,
        )

class MayaviWidget(QtGui.QWidget):
    """Container class for Mayavi visualization widget."""
    def __init__(self, parent=None, f=0):
        super(MayaviWidget, self).__init__(parent, f)
        # add a pointless layout
        layout = QtGui.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.visualization = Visualization()

        # The edit_traits call will generate the widget to embed.
        self.ui = self.visualization.edit_traits(parent=self,
                                                 kind='subpanel').control
        layout.addWidget(self.ui)
        self.ui.setParent(self)

    def graph(self, discrete_values, limits):
        """Right now, simply a proxy function.

        .. todo::

            Possibly eliminate the proxy.
        
        .. seealso:: :meth:`Visualization.graph()`

        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 2
            The dependent variable is autoscaled.
        """
        self.visualization.graph(discrete_values, limits)
