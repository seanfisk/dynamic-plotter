""":mod:dynamic_plotter.gui.backends.matplotlib --- Matplotlib renderer
"""

# avoid matplotlib module name clash
from __future__ import absolute_import

from pyface.qt import QtCore, QtGui

# matplotlib imports
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
# del matplotlib # not needed anymore
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from mpl_toolkits.mplot3d import Axes3D

from dynamic_plotter.gui.backends.base import BackendBaseWidget

import numpy as np

class IncorrectLimitDimensionsError(Exception):
    """Exception raised when incorrect dimensions are given with the limits."""
    def __init__(self, limits):
        """Creat the error.
        
        :param limits: incorrect limits
        :type limits: :class:`list` of :class:`tuple`
        """
        self.limits = limits

    def __str__(self):
        """Stringify the error.

        :return: the string representation of the error
        :rtype: :class:`str`
        """
        return 'Incorrect dimensions given for graph limit: ' + str(self.limits)
        

class MatplotlibWidget(BackendBaseWidget):
    """Render a graph using matplotlib and PySide.

    .. todo::

        Consolidate 2D and 3D functions.

    .. todo::

        Don't re-create the figure each time.
        
    """
    def __init__(self, parent=None, f=0):
        super(MatplotlibWidget, self).__init__(parent, f)
        
        # matplotlib setup
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)

        # gui setup
        self.layout = QtGui.QVBoxLayout(self)
        self.layout.addWidget(self.canvas)

    def graph(self, discrete_values, limits):
        """Graph a 2D or 3D function. Currently a stub in lieu of Kurt's
        uncommited code.

        .. todo::

            Check whether :meth:`self.canvas.draw()` call is necessary.
        
        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 1 or 2
            len(limits) == 1 is 2D graph; len(limits) == 2 is 3D graph.
            The dependent variable is autoscaled.
        """
        if len(limits) == 1:
            self._graph_2D(discrete_values, limits)
        elif len(limits) == 2:
            self._graph_3D(discrete_values, limits)
        else:
            raise IncorrectLimitDimensionsError(limits)
        # for rendering one graph, doesn't appear to be necessary
        self.canvas.draw()

    def _graph_2D(self, discrete_values, limits):
        """Graph a 2D function.

        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 1
        """
        axes = self.figure.add_subplot(111)
        axes.plot(
            discrete_values[0],
            discrete_values[1],
            # not necessary, but would probably speed up rendering
            scalex=False,
            )
        # set limits here to avoid any possible auto-scaling behavior
        axes.set_xlim(limits[0])

    def _graph_3D(self, discrete_values, limits):
        """Graph a 2D function.

        :param discrete_values: discrete values to graph
        :type discrete_values: :class:`list` of :class:`list`
        :param limits: minimum and maximum values to graph
        :type limits: :class:`list` of (:class:`tuple` of length 2) of length 2
        """
        # axes = Axes3D()
        # self.figure.add_axes(axes)
        axes = self.figure.add_subplot(111, projection='3d')
        axes.plot(
            xs=discrete_values[0],
            ys=discrete_values[1],
            zs=discrete_values[2],
            # not necessary, but would probably speed up rendering
            # scalex=False,
            # scaley=False,
            )
        # set limits here to avoid any possible auto-scaling behavior
        axes.set_xlim(limits[0])
        axes.set_ylim(limits[1])

