""":mod:`dynamic_plotter.window` -- Graphical graphical representation of
functions

Provide a graphical user in-your-face interface for the dynamic plotter.

.. todo::

    Now have to "quit" the application once before the window shows. It's an
    annoyance. Pretty sure it has to deal with the pyface.qt / PySide issue and
    more than one QApplication being created. (This doesn't seem to be happening
    anymore, but still check it out)

.. todo::

    Refactor the tab widget and the backend chooser. It is pretty dirty as of
    now.

.. todo::

    Allow 2D graphs in matplotlib and 3D graphs in matplotlib or Mayavi.

"""

from __future__ import print_function, division
from dynamic_plotter import metadata
from pyface.qt import QtCore, QtGui
from dynamic_plotter.core import discretize

from dynamic_plotter.gui.backends.mayavi import MayaviWidget
from dynamic_plotter.gui.backends.matplotlib import MatplotlibWidget
# from dynamic_plotter.gui.backends.base import BackendBaseWidget

BACKEND_OPTIONS = ['matplotlib', 'mayavi']

class InputButton(QtGui.QPushButton):
    """Button which can emit a signal that includes the button's text."""
    
    clicked_with_text = QtCore.Signal(str)
    """A signal which indicates that a button has been clicked and also gives
    its text."""

    def __init__(self, text, parent=None):
        """Create a new button."""
        super(InputButton, self).__init__(text, parent)
        self.clicked.connect(self.emit_clicked_with_text)
        self.setFocusPolicy(QtCore.Qt.NoFocus)

    @QtCore.Slot()
    def emit_clicked_with_text(self):
        """Emit :attr:`self.clicked_with_text` with an argument of this button's
        text (:meth:`self.text()`)."""
        self.clicked_with_text.emit(self.text())

class BackendSelectorWidget(QtGui.QWidget):
    """Widget to select a backend."""
    def __init__(self, parent=None, f=0):
        """Create the selector widget."""
        super(BackendSelectorWidget, self).__init__(parent, f)
        self.button_group = QtGui.QButtonGroup()
        self.layout = QtGui.QVBoxLayout(self)
        self.radio_buttons = []
        for id, option in enumerate(BACKEND_OPTIONS):
            button = QtGui.QRadioButton(option.capitalize())
            self.button_group.addButton(button, id)
            self.layout.addWidget(button)
            self.radio_buttons.append(button)
        self.radio_buttons[0].setChecked(True)

    def selection(self):
        """Return the selected backend.

        :return: the selected backend
        :rtype: :class:`str`
        """
        return self.button_group.checkedId()
    
class AboutDialog(QtGui.QDialog):
    """Shows information about the program."""
    def __init__(self, parent=None):
        """Construct the dialog."""
        super(AboutDialog, self).__init__(parent)
        self.setWindowTitle('About ' + metadata.nice_title)
        self.about_layout = QtGui.QVBoxLayout()
        self.title_label = QtGui.QLabel(metadata.nice_title)
        self.ver_label = QtGui.QLabel('Version ' + metadata.version)
        self.cr_label = QtGui.QLabel('Copyright (C) ' + metadata.copyright)
        self.url_label = QtGui.QLabel(
            '<a href="{0}">{0}</a>'.format(metadata.url))
        self.url_label.setOpenExternalLinks(True)
        self.about_layout.addWidget(self.title_label)
        self.about_layout.addWidget(self.ver_label)
        self.about_layout.addWidget(self.cr_label)
        self.about_layout.addWidget(self.url_label)
        for i in xrange(self.about_layout.count()):
            self.about_layout.itemAt(i).setAlignment(QtCore.Qt.AlignHCenter)
        self.setLayout(self.about_layout)

class MainWindow(QtGui.QMainWindow):
    """Application's main window."""

    PADDED_SYMBOLS = ['+', '-', '*', '/', '**', 'x', 'y']
    # use `_i' instead of `i' so variable isn't documented
    SYMBOLS = PADDED_SYMBOLS + ['.', '(', ')'] + [str(_i) for _i in xrange(0, 10)]
    
    def __init__(self, parent=None):
        """Create the main window."""
        super(MainWindow, self).__init__(parent)
        self.setWindowTitle(metadata.nice_title)

        # menu bar
        self.setMenuBar(QtGui.QMenuBar())
        
        self.file_menu = QtGui.QMenu('File')
        self.exit_menu_action = self.file_menu.addAction('Exit')
        self.exit_menu_action.triggered.connect(self.close)
        self.menuBar().addMenu(self.file_menu)
        
        self.help_menu = QtGui.QMenu('Help')
        self.about_menu_action = self.help_menu.addAction('About')
        self.about_menu_action.triggered.connect(self.about)
        self.menuBar().addMenu(self.help_menu)

        # main content
        self.setCentralWidget(QtGui.QWidget())
        self.main_layout = QtGui.QHBoxLayout(self.centralWidget())
                        
        ## left pane
        self.left_pane = QtGui.QTabWidget()
        self.main_layout.addWidget(self.left_pane)

        ### entry tab
        self.entry_tab = QtGui.QWidget()
        self.left_pane.addTab(self.entry_tab, 'Entry')
        self.input_form_layout = QtGui.QFormLayout(self.entry_tab)

        self.backend_selector = BackendSelectorWidget()
        self.input_form_layout.addRow(self.backend_selector)

        self.input_buttons_layout = QtGui.QGridLayout()
        widgets_per_row = 5
        for i, text in enumerate(self.SYMBOLS):
            button = InputButton(text)
            button.clicked_with_text.connect(self.input_button_clicked)
            self.input_buttons_layout.addWidget(button, i // widgets_per_row,
                                                i % widgets_per_row)
        self.input_form_layout.addRow(self.input_buttons_layout)
        
        self.function_field = QtGui.QLineEdit()
        self.input_form_layout.addRow('Function', self.function_field)
        
        self.range_fields = []
        for dimension in ['X', 'Y']:
            extrema_fields = []
            for extrema in ['min', 'max']:
                field = QtGui.QLineEdit()
                extrema_fields.append(field)
                self.input_form_layout.addRow(
                    '{0} {1}'.format(dimension, extrema), field)
            self.range_fields.append(tuple(extrema_fields))
        
        self.visualize_button = QtGui.QPushButton('Visualize')
        self.visualize_button.clicked.connect(self.visualize)
        self.input_form_layout.addRow(self.visualize_button)
        
        ### other tab
        self.other_tab = QtGui.QWidget()
        self.left_pane.addTab(self.other_tab, 'Other')
        
        ## right pane
        # self.render_widget = MayaviWidget()
        # self.render_widget = MatplotlibWidget()
        self.right_stack = QtGui.QStackedWidget()
        self.right_stack.addWidget(MatplotlibWidget())
        self.right_stack.addWidget(MayaviWidget())

        # sample 2D data
        # data = np.array([
        #     [1, 2, 3, 4, 5],
        #     [10, 20, 30, 40, 50]])
        # limits = [(-11, 32)]

        # sample 3D data
        # data = np.array([
        #     [1, 2, 3, 4, 5],
        #     [10, 20, 30, 40, 50],
        #     [5, 10, 15, 20, 25]])
        # limits = [(0, 100), (0, 60)]

        # self.render_widget.graph(data, limits)
        self.main_layout.addWidget(self.right_stack)
                
    @QtCore.Slot()
    def about(self):
        """Create and show the about dialog."""
        AboutDialog(self).exec_()

    @QtCore.Slot()
    def visualize(self):
        """Visualize the function in the function box.

        .. todo::

            Clean up the implementation.

        """
        function_str = self.function_field.text()
        x_min = eval(self.range_fields[0][0].text())
        x_max = eval(self.range_fields[0][1].text())
        y_min = eval(self.range_fields[1][0].text())
        y_max = eval(self.range_fields[1][1].text())
        # print(function_str)
        # print(x_min)
        # print(x_max)
        # print(y_min)
        # print(y_max)
        # limits = [(x_min, x_max)]
        selection = self.backend_selector.selection()
        self.right_stack.setCurrentIndex(selection)
        limits = [(x_min, x_max), (y_min, y_max)]
        discrete_values = discretize.three_d(function_str, limits)
        self.right_stack.widget(selection).graph(discrete_values, limits)
        
        # call a method (not yet implemented) to update the xyz at origin here
        # need more info for implementation details
        
    @QtCore.Slot(str)
    def input_button_clicked(self, text):
        """Slot triggered when an input button is clicked.

        :param text: the button text
        :type text: :class:`str`"""
        # QApplication.focusWidget() returns the currently focused widget
        focus_widget = QtGui.QApplication.focusWidget()
        if type(focus_widget) == QtGui.QLineEdit:
            pos = focus_widget.cursorPosition()
            if text in self.PADDED_SYMBOLS:
                inserted_text = ' ' + text + ' '
            else:
                inserted_text = text
            old_text = focus_widget.text()
            new_text = old_text[:pos] + inserted_text + old_text[pos:]
            focus_widget.setText(new_text)
            pos += len(inserted_text)
            focus_widget.setCursorPosition(pos)
