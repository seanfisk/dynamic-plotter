""":mod:`dynamic_plotter.core.discretize` -- Function evaluation code

.. todo::

    Consolidate repetitive code in 2D / 3D.

.. todo::

    Take care of the horrible state of the imports.
    
"""

from __future__ import division
import math

MATH_IMPORTS = ['sin', 'cos', 'tan', 'asin', 'acos', 'atan', 'log', 'log10',
                'fabs', 'factorial', 'pi', 'e']

# temp_math_module = __import__('math', None, None, MATH_IMPORTS, 0)
# from temp_math_module import *

# from importlib import import_module
# import math

STEP = 25

def two_d(string, limits):
    """Transform a 2D function into discrete data.

    :param string: the function string
    :type string: :class:`str`
    :param limits: the limits to graph
    :type limits: :class:`list` of :class:`list`
    :return: the discrete data
    :rtype: :class:`list` of :class:`list`
    """
    def f(x):
        from math import (sin, cos, tan, asin, acos, atan, log, log10, fabs,
                          factorial, pi, e)
        return eval(string)
    x_step = math.fabs(limits[0][1] - limits[0][0]) / STEP
    x = []
    y = []

    for i in xrange(STEP + 1):
        try:
            y.append(f(limits[0][0] + i * x_step))
            x.append(limits[0][0] + i * x_step)
        except ValueError:
            pass

    return [x, y]


def three_d(string, limits):
    """Transform a 3D function into discrete data.

    :param string: the function string
    :type string: :class:`str`
    :param limits: the limits to graph
    :type limits: :class:`list` of :class:`list`
    :return: the discrete data
    :rtype: :class:`list` of :class:`list`
    """
    def f(x,y):
        from math import (sin, cos, tan, asin, acos, atan, log, log10, fabs,
                          factorial, pi, e)
        return eval(string)

    x_step = math.fabs(limits[0][1] - limits[0][0]) / STEP
    y_step = math.fabs(limits[1][1] - limits[1][0]) / STEP
    x = []
    y = []
    z = []

    for i in xrange(STEP + 1):
        for j in xrange(STEP + 1):
            try:
                # calculate z first in case of error
                temp_z = f(limits[0][0] + i * x_step, limits[1][0] + j * y_step)
                temp_x = limits[0][0] + i * x_step
                temp_y = limits[1][0] + j * y_step

                x.append(temp_x)
                y.append(temp_y)
                z.append(temp_z)
            except ValueError:
                pass

    return [x, y, z]
