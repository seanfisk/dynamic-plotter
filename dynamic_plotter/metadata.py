""":mod:`my_module.metadata` --- Project metadata

Information describing the project.
"""

title = 'dynamic_plotter'
nice_title = "Dynamic Plotter"
nice_title_no_spaces = nice_title.replace(' ', '')
version = '0.1'
description = 'Graphical representation of functions.'
authors = ['James Donley', 'Sean Fisk', 'Kurt O\'Hearn']
authors_string = ', '.join(authors)
emails = [username + '@mail.gvsu.edu' 
          for username in ['donleyj', 'fiskse', 'ohearnk']]
license = 'ISC'
copyright = '2012 ' + authors_string
url = 'http://cis.gvsu.edu/'
